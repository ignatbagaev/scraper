require 'rails_helper'

feature 'root path' do
  scenario 'shows welcome message' do
    visit root_path
    expect(page).to have_content 'Hello Scraper'
  end
end
