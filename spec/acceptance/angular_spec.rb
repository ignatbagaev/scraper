require 'rails_helper'

feature 'angular test' do
  scenario 'angular works as expected', js: true do
    visit root_path
    fill_in :test, with: 'Angular test'
    # save_and_open_page
    within('p.test') do
      expect(page).to have_content 'Angular test'
    end
  end
end
